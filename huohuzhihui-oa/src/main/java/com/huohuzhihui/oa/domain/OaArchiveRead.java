package com.huohuzhihui.oa.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import com.huohuzhihui.system.domain.SysFiles;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 轮阅记录对象 oa_archive_read
 * 
 * @author yepanpan
 * @date 2020-12-15
 */
@ApiModel("轮阅记录实体")
@Getter
@Setter
public class OaArchiveRead extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;

    /** 文档 */
    @Excel(name = "文档")
    @ApiModelProperty("文档")
    private Long archiveId;
    private OaArchive archive;

    private SysFiles file;

    /** 人员ID */
    @Excel(name = "人员ID")
    @ApiModelProperty("人员ID")
    private Long userId;

    /** 姓名 */
    @Excel(name = "姓名")
    @ApiModelProperty("姓名")
    private String name;

    /** 阅读时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "阅读时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("阅读时间")
    private Date readTime;



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("archiveId", getArchiveId())
            .append("userId", getUserId())
            .append("name", getName())
            .append("readTime", getReadTime())
            .toString();
    }
}
