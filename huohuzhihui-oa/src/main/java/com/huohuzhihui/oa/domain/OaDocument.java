package com.huohuzhihui.oa.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import com.huohuzhihui.system.domain.SysFiles;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * 公文对象 oa_document
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@ApiModel("公文实体")
@Getter
@Setter
public class OaDocument extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;

    /** 工作流 */
    @ApiModelProperty("工作流")
    @NotNull(message = "必须选择工作流程")
    private Long flowId;
    @Excel(name = "工作流")
    private String flowName;

    @ApiModelProperty("表单")
    private List<OaForm> form;

    /** 当前步骤 */
    @ApiModelProperty("当前步骤")
    private Long stepId;
    @Excel(name = "当前步骤")
    private String stepName;

    /**发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("发起时间")
    private Date addTime;

    /** 发起人 */
    @ApiModelProperty("发起人")
    private Long addUser;
    @Excel(name = "发起人")
    private String addUserName;

    /** 状态 */
    @Excel(name = "状态", dictType = "flow_status")
    @ApiModelProperty("状态")
    private String status;

    /** 附件 */
    @ApiModelProperty("附件")
    private Long fileId;
    private SysFiles file;
    

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("flowId", getFlowId())
            .append("stepId", getStepId())
            .append("addTime", getAddTime())
            .append("addUser", getAddUser())
            .append("updateTime", getUpdateTime())
            .append("status", getStatus())
            .toString();
    }
}
