package com.huohuzhihui.oa.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import com.huohuzhihui.system.domain.SysFiles;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 工作汇报对象 oa_report
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@ApiModel("工作汇报实体")
@Getter
@Setter
public class OaReport extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;

    /** 年度 */
    @Excel(name = "年度")
    @ApiModelProperty("年度")
    private Long year;

    /** 分类 */
    @Excel(name = "分类", dictType = "report_type")
    @ApiModelProperty("分类")
    private String type;

    /** 标题 */
    @Excel(name = "标题")
    @ApiModelProperty("标题")
    private String title;

    /** 汇报内容 */
    @Excel(name = "汇报内容")
    @ApiModelProperty("汇报内容")
    private String content;

    /** 附件 */
    @ApiModelProperty("附件")
    private Long fileId;

    private SysFiles file;

    /** 汇报时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "汇报时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("汇报时间")
    private Date addTime;

    /** 汇报人 */
    @ApiModelProperty("汇报人")
    private Long addUser;
    @Excel(name = "汇报人")
    private String addUserName;

    /** 审阅时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "审阅时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("审阅时间")
    private Date readTime;

    /** 审阅人 */
    @ApiModelProperty("审阅人")
    private Long toUser;
    @Excel(name = "审阅人")
    private String toUserName;



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("year", getYear())
            .append("type", getType())
            .append("title", getTitle())
            .append("content", getContent())
            .append("fileId", getFileId())
            .append("addTime", getAddTime())
            .append("addUser", getAddUser())
            .append("readTime", getReadTime())
            .append("toUser", getToUser())
            .toString();
    }
}
