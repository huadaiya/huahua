package com.huohuzhihui.oa.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 工作流步骤对象 oa_flow_process
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@ApiModel("工作流步骤实体")
@Getter
@Setter
public class OaFlowProcess extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;

    /** 工作流 */
    @Excel(name = "工作流")
    @ApiModelProperty("工作流")
    private Long flowId;
    private String flowName;

    /** 公文 */
    @Excel(name = "公文")
    @ApiModelProperty("公文")
    private Long documentId;    
    private OaDocument doc;

    /** 上一步 */
    @Excel(name = "上一步")
    @ApiModelProperty("上一步")
    private Long prevId;

    /** 当前步骤 */
    @Excel(name = "当前步骤")
    @ApiModelProperty("当前步骤")
    private Long stepId;

    /** 步骤名称 */
    @Excel(name = "步骤名称")
    @ApiModelProperty("步骤名称")
    private String title;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty("状态")
    private String status;

    /** 添加时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "添加时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("添加时间")
    private Date addTime;

    /** 当前处理人 */
    @Excel(name = "当前处理人")
    @ApiModelProperty("当前处理人")
    private Long userId;
    private String processUser;
    
    /**
     * 意见
     */
    private String comment;


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("flowId", getFlowId())
            .append("documentId", getDocumentId())
            .append("prevId", getPrevId())
            .append("stepId", getStepId())
            .append("title", getTitle())
            .append("status", getStatus())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("userId", getUserId())
            .toString();
    }
}
