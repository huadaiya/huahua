package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.common.utils.ServletUtils;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaArchiveRead;
import com.huohuzhihui.oa.service.IOaArchiveReadService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 轮阅记录Controller
 * 
 * @author yepanpan
 * @date 2020-12-15
 */
@Api("轮阅记录信息管理")
@RestController
@RequestMapping("/oa/read")
public class OaArchiveReadController extends BaseController
{
    @Autowired
    private IOaArchiveReadService oaArchiveReadService;

    /**
     * 查询轮阅记录列表
     */
    @ApiOperation("获取轮阅记录列表")
    @PreAuthorize("@ss.hasPermi('oa:read:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaArchiveRead oaArchiveRead)
    {
        startPage();
        oaArchiveRead.getParams().put("title", ServletUtils.getParameter("title"));
        oaArchiveRead.getParams().put("status", ServletUtils.getParameter("status"));
        List<OaArchiveRead> list = oaArchiveReadService.selectOaArchiveReadList(oaArchiveRead);
        return getDataTable(list);
    }
    

    /**
     * 查询个人的轮阅记录列表
     */
    @ApiOperation("获取个人的轮阅记录列表")
    @PreAuthorize("@ss.hasPermi('oa:read:my')")
    @GetMapping("/my")
    public TableDataInfo my(OaArchiveRead oaArchiveRead)
    {
        startPage();
        oaArchiveRead.getParams().put("title", ServletUtils.getParameter("title"));
        oaArchiveRead.getParams().put("status", ServletUtils.getParameter("status"));
        oaArchiveRead.setUserId(SecurityUtils.getLoginUser().getUser().getUserId());
        List<OaArchiveRead> list = oaArchiveReadService.selectOaArchiveReadList(oaArchiveRead);
        return getDataTable(list);
    }

    /**
     * 导出轮阅记录列表
     */
    @ApiOperation("导出轮阅记录列表")
    @PreAuthorize("@ss.hasPermi('oa:read:export')")
    @Log(title = "轮阅记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaArchiveRead oaArchiveRead)
    {
        List<OaArchiveRead> list = oaArchiveReadService.selectOaArchiveReadList(oaArchiveRead);
        ExcelUtil<OaArchiveRead> util = new ExcelUtil<OaArchiveRead>(OaArchiveRead.class);
        return util.exportExcel(list, "read");
    }

    /**
     * 获取轮阅记录详细信息
     */
    @ApiOperation("获取轮阅记录详细信息")
    @ApiImplicitParam(name = "id", value = "轮阅记录ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:read:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaArchiveReadService.selectOaArchiveReadById(id));
    }

    /**
     * 新增轮阅记录
     */
    @ApiOperation("新增轮阅记录")
    @PreAuthorize("@ss.hasPermi('oa:read:add')")
    @Log(title = "轮阅记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaArchiveRead oaArchiveRead)
    {
        return toAjax(oaArchiveReadService.insertOaArchiveRead(oaArchiveRead));
    }

    /**
     * 修改轮阅记录
     */
    @ApiOperation("修改轮阅记录")
    @PreAuthorize("@ss.hasPermi('oa:read:edit')")
    @Log(title = "轮阅记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaArchiveRead oaArchiveRead)
    {
        return toAjax(oaArchiveReadService.updateOaArchiveRead(oaArchiveRead));
    }

    /**
     * 删除轮阅记录
     */
    @ApiOperation("删除轮阅记录")
    @ApiImplicitParam(name = "id", value = "轮阅记录ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:read:remove')")
    @Log(title = "轮阅记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaArchiveReadService.deleteOaArchiveReadByIds(ids));
    }
    
    /**
     * 标记已读
     */
    @ApiOperation("标记已读")
    @ApiImplicitParam(name = "id", value = "轮阅记录ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:read:remove')")
    @Log(title = "轮阅记录", businessType = BusinessType.DELETE)
	@GetMapping("/mark/{ids}")
    public AjaxResult mark(@PathVariable Long[] ids)
    {
        return toAjax(oaArchiveReadService.markRead(ids));
    }
}
