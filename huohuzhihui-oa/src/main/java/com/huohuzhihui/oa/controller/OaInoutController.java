package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaInout;
import com.huohuzhihui.oa.service.IOaInoutService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 出入记录Controller
 * 
 * @author yepanpan
 * @date 2020-12-10
 */
@Api("出入记录信息管理")
@RestController
@RequestMapping("/oa/inout")
public class OaInoutController extends BaseController
{
    @Autowired
    private IOaInoutService oaInoutService;

    /**
     * 查询出入记录列表
     */
    @ApiOperation("获取出入记录列表")
    @PreAuthorize("@ss.hasPermi('oa:inout:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaInout oaInout)
    {
        startPage();
        List<OaInout> list = oaInoutService.selectOaInoutList(oaInout);
        return getDataTable(list);
    }

    /**
     * 导出出入记录列表
     */
    @ApiOperation("导出出入记录列表")
    @PreAuthorize("@ss.hasPermi('oa:inout:export')")
    @Log(title = "出入记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaInout oaInout)
    {
        List<OaInout> list = oaInoutService.selectOaInoutList(oaInout);
        ExcelUtil<OaInout> util = new ExcelUtil<OaInout>(OaInout.class);
        return util.exportExcel(list, "出入记录");
    }

    /**
     * 获取出入记录详细信息
     */
    @ApiOperation("获取出入记录详细信息")
    @ApiImplicitParam(name = "id", value = "出入记录ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:inout:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaInoutService.selectOaInoutById(id));
    }

    /**
     * 新增出入记录
     */
    @ApiOperation("新增出入记录")
    @PreAuthorize("@ss.hasPermi('oa:inout:add')")
    @Log(title = "出入记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaInout oaInout)
    {
        return toAjax(oaInoutService.insertOaInout(oaInout));
    }

    /**
     * 修改出入记录
     */
    @ApiOperation("修改出入记录")
    @PreAuthorize("@ss.hasPermi('oa:inout:edit')")
    @Log(title = "出入记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaInout oaInout)
    {
        return toAjax(oaInoutService.updateOaInout(oaInout));
    }

    /**
     * 删除出入记录
     */
    @ApiOperation("删除出入记录")
    @ApiImplicitParam(name = "id", value = "出入记录ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:inout:remove')")
    @Log(title = "出入记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaInoutService.deleteOaInoutByIds(ids));
    }
}
