package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaArchive;
import com.huohuzhihui.oa.service.IOaArchiveService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 文档轮阅Controller
 * 
 * @author yepanpan
 * @date 2020-12-15
 */
@Api("文档轮阅信息管理")
@RestController
@RequestMapping("/oa/archive")
public class OaArchiveController extends BaseController
{
    @Autowired
    private IOaArchiveService oaArchiveService;

    /**
     * 查询文档轮阅列表
     */
    @ApiOperation("获取文档轮阅列表")
    @PreAuthorize("@ss.hasPermi('oa:archive:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaArchive oaArchive)
    {
        startPage();
        List<OaArchive> list = oaArchiveService.selectOaArchiveList(oaArchive);
        return getDataTable(list);
    }

    /**
     * 导出文档轮阅列表
     */
    @ApiOperation("导出文档轮阅列表")
    @PreAuthorize("@ss.hasPermi('oa:archive:export')")
    @Log(title = "文档轮阅", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaArchive oaArchive)
    {
        List<OaArchive> list = oaArchiveService.selectOaArchiveList(oaArchive);
        ExcelUtil<OaArchive> util = new ExcelUtil<OaArchive>(OaArchive.class);
        return util.exportExcel(list, "文件轮阅");
    }

    /**
     * 获取文档轮阅详细信息
     */
    @ApiOperation("获取文档轮阅详细信息")
    @ApiImplicitParam(name = "id", value = "文档轮阅ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:archive:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaArchiveService.selectOaArchiveById(id));
    }

    /**
     * 新增文档轮阅
     */
    @ApiOperation("新增文档轮阅")
    @PreAuthorize("@ss.hasPermi('oa:archive:add')")
    @Log(title = "文档轮阅", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaArchive oaArchive)
    {
        return toAjax(oaArchiveService.insertOaArchive(oaArchive));
    }

    /**
     * 修改文档轮阅
     */
    @ApiOperation("修改文档轮阅")
    @PreAuthorize("@ss.hasPermi('oa:archive:edit')")
    @Log(title = "文档轮阅", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaArchive oaArchive)
    {
        return toAjax(oaArchiveService.updateOaArchive(oaArchive));
    }

    /**
     * 删除文档轮阅
     */
    @ApiOperation("删除文档轮阅")
    @ApiImplicitParam(name = "id", value = "文档轮阅ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:archive:remove')")
    @Log(title = "文档轮阅", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaArchiveService.deleteOaArchiveByIds(ids));
    }
}
