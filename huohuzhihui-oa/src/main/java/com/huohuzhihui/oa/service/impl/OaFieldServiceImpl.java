package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.oa.domain.OaField;
import com.huohuzhihui.oa.mapper.OaFieldMapper;
import com.huohuzhihui.oa.service.IOaFieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 单字段Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Service
public class OaFieldServiceImpl implements IOaFieldService 
{
    @Autowired
    private OaFieldMapper oaFieldMapper;

    /**
     * 查询单字段
     * 
     * @param id 单字段ID
     * @return 单字段
     */
    @Override
    public OaField selectOaFieldById(Long id)
    {
        return oaFieldMapper.selectOaFieldById(id);
    }

    /**
     * 查询单字段列表
     * 
     * @param oaField 单字段
     * @return 单字段
     */
    @Override
    public List<OaField> selectOaFieldList(OaField oaField)
    {
        return oaFieldMapper.selectOaFieldList(oaField);
    }

    /**
     * 新增单字段
     * 
     * @param oaField 单字段
     * @return 结果
     */
    @Override
    public int insertOaField(OaField oaField)
    {
        return oaFieldMapper.insertOaField(oaField);
    }

    /**
     * 修改单字段
     * 
     * @param oaField 单字段
     * @return 结果
     */
    @Override
    public int updateOaField(OaField oaField)
    {
        return oaFieldMapper.updateOaField(oaField);
    }

    /**
     * 批量删除单字段
     * 
     * @param ids 需要删除的单字段ID
     * @return 结果
     */
    @Override
    public int deleteOaFieldByIds(Long[] ids)
    {
        return oaFieldMapper.deleteOaFieldByIds(ids);
    }

    /**
     * 删除单字段信息
     * 
     * @param id 单字段ID
     * @return 结果
     */
    @Override
    public int deleteOaFieldById(Long id)
    {
        return oaFieldMapper.deleteOaFieldById(id);
    }
}
