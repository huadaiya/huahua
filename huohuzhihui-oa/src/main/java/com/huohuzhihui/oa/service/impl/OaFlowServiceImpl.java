package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.oa.domain.OaFlow;
import com.huohuzhihui.oa.mapper.OaFlowMapper;
import com.huohuzhihui.oa.service.IOaFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工作流Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Service
public class OaFlowServiceImpl implements IOaFlowService 
{
    @Autowired
    private OaFlowMapper oaFlowMapper;

    /**
     * 查询工作流
     * 
     * @param id 工作流ID
     * @return 工作流
     */
    @Override
    public OaFlow selectOaFlowById(Long id)
    {
        return oaFlowMapper.selectOaFlowById(id);
    }

    /**
     * 查询工作流列表
     * 
     * @param oaFlow 工作流
     * @return 工作流
     */
    @Override
    public List<OaFlow> selectOaFlowList(OaFlow oaFlow)
    {
        return oaFlowMapper.selectOaFlowList(oaFlow);
    }

    /**
     * 新增工作流
     * 
     * @param oaFlow 工作流
     * @return 结果
     */
    @Override
    public int insertOaFlow(OaFlow oaFlow)
    {
    	oaFlow.setCreateBy(SecurityUtils.getUsername());
        oaFlow.setCreateTime(DateUtils.getNowDate());
        return oaFlowMapper.insertOaFlow(oaFlow);
    }

    /**
     * 修改工作流
     * 
     * @param oaFlow 工作流
     * @return 结果
     */
    @Override
    public int updateOaFlow(OaFlow oaFlow)
    {
    	oaFlow.setUpdateBy(SecurityUtils.getUsername());
        oaFlow.setUpdateTime(DateUtils.getNowDate());
        return oaFlowMapper.updateOaFlow(oaFlow);
    }

    /**
     * 批量删除工作流
     * 
     * @param ids 需要删除的工作流ID
     * @return 结果
     */
    @Override
    public int deleteOaFlowByIds(Long[] ids)
    {
        return oaFlowMapper.deleteOaFlowByIds(ids);
    }

    /**
     * 删除工作流信息
     * 
     * @param id 工作流ID
     * @return 结果
     */
    @Override
    public int deleteOaFlowById(Long id)
    {
        return oaFlowMapper.deleteOaFlowById(id);
    }
}
