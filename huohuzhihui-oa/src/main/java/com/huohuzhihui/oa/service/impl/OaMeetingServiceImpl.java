package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.common.utils.bean.BeanUtils;
import com.huohuzhihui.oa.domain.OaMeeting;
import com.huohuzhihui.oa.domain.OaMeetingUser;
import com.huohuzhihui.oa.mapper.OaMeetingMapper;
import com.huohuzhihui.oa.mapper.OaMeetingUserMapper;
import com.huohuzhihui.oa.service.IOaMeetingService;
import com.huohuzhihui.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 会议申请Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Service
public class OaMeetingServiceImpl implements IOaMeetingService 
{
    @Autowired
    private OaMeetingMapper oaMeetingMapper;
    @Autowired
    private OaMeetingUserMapper oaMeetingUserMapper;
    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 查询会议申请
     * 
     * @param id 会议申请ID
     * @return 会议申请
     */
    @Override
    public OaMeeting selectOaMeetingById(Long id)
    {
    	OaMeeting oaMeeting = oaMeetingMapper.selectOaMeetingById(id);
    	OaMeetingUser uc = new OaMeetingUser();
    	uc.setMeetId(id);
    	List<OaMeetingUser> list = oaMeetingUserMapper.selectOaMeetingUserList(uc);
    	if(list != null && list.size() > 0) {
    		Long users[] = new Long[list.size()];
    		String userNames = "";
    		for(int i=0;i<list.size();i++) {
    			users[i] = list.get(i).getUserId();
    			userNames += "," + list.get(i).getName();
    		}
    		oaMeeting.setUsers(users);
    		oaMeeting.setUserNames(userNames.substring(1));
    	}
    	return oaMeeting;
    }

    /**
     * 查询会议申请列表
     * 
     * @param oaMeeting 会议申请
     * @return 会议申请
     */
    @Override
    public List<OaMeeting> selectOaMeetingList(OaMeeting oaMeeting)
    {
        return oaMeetingMapper.selectOaMeetingList(oaMeeting);
    }

    /**
     * 新增会议申请
     * 
     * @param oaMeeting 会议申请
     * @return 结果
     */
    @Override
    @Transactional
    public int insertOaMeeting(OaMeeting oaMeeting)
    {
    	oaMeeting.setAddUser(SecurityUtils.getLoginUser().getUser().getUserId());
    	oaMeeting.setAddTime(DateUtils.getNowDate());
        int count = oaMeetingMapper.insertOaMeeting(oaMeeting);
        if(oaMeeting.getUsers() != null || oaMeeting.getUsers().length > 0) {
        	for(Long userId : oaMeeting.getUsers()) {
        		SysUser u = sysUserMapper.selectUserById(userId);
        		OaMeetingUser mu = new OaMeetingUser();
        		mu.setUserId(userId);
        		mu.setMeetId(oaMeeting.getId());
        		mu.setName(u.getNickName());
        		mu.setPhone(u.getPhonenumber());
        		mu.setIsSign("N");
        		oaMeetingUserMapper.insertOaMeetingUser(mu);
        		count ++;
        	}
        }
        return count;
    }

    /**
     * 修改会议申请
     * 
     * @param oaMeeting 会议申请
     * @return 结果
     */
    @Override
    @Transactional
    public int updateOaMeeting(OaMeeting oaMeeting)
    {
    	int count =  oaMeetingMapper.updateOaMeeting(oaMeeting);
    	if(oaMeeting.getUsers() != null || oaMeeting.getUsers().length > 0) {
    		oaMeetingUserMapper.deleteByMeeting(oaMeeting.getId());
    		
        	for(Long userId : oaMeeting.getUsers()) {
        		SysUser u = sysUserMapper.selectUserById(userId);
        		OaMeetingUser mu = new OaMeetingUser();
        		mu.setUserId(userId);
        		mu.setMeetId(oaMeeting.getId());
        		mu.setName(u.getNickName());
        		mu.setPhone(u.getPhonenumber());
        		mu.setIsSign("N");
        		oaMeetingUserMapper.insertOaMeetingUser(mu);
        		count ++;
        	}
        }
        return count;
    }

    /**
     * 批量删除会议申请
     * 
     * @param ids 需要删除的会议申请ID
     * @return 结果
     */
    @Override
    public int deleteOaMeetingByIds(Long[] ids)
    {
        return oaMeetingMapper.deleteOaMeetingByIds(ids);
    }

    /**
     * 删除会议申请信息
     * 
     * @param id 会议申请ID
     * @return 结果
     */
    @Override
    public int deleteOaMeetingById(Long id)
    {
        return oaMeetingMapper.deleteOaMeetingById(id);
    }
    /**
     * 审核会议申请
     * 
     * @param oaMeeting 会议申请
     * @return 结果
     */
    @Override
    @Transactional
    public int checkMeeting(OaMeeting oaMeeting) {
    	oaMeeting.setCheckUser(SecurityUtils.getLoginUser().getUser().getUserId());
    	oaMeeting.setCheckTime(DateUtils.getNowDate());
    	Long[] batchs = oaMeeting.getBatchs();
        int i = 0;
        if(batchs != null) {
        	for(Long id : batchs) {
        		OaMeeting vo = new OaMeeting();
        		BeanUtils.copyProperties(oaMeeting, vo);
        		vo.setId(id);
        		oaMeetingMapper.updateOaMeeting(vo);
        		i++;
        	}
        }
        if(oaMeeting.getId() != null) {
        	oaMeetingMapper.updateOaMeeting(oaMeeting);
    		i++;
        }
        return i;
    }
}
