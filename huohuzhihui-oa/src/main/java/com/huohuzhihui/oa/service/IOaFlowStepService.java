package com.huohuzhihui.oa.service;

import com.huohuzhihui.oa.domain.OaFlowStep;

import java.util.List;

/**
 * 工作流步骤Service接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface IOaFlowStepService 
{
    /**
     * 查询工作流步骤
     * 
     * @param id 工作流步骤ID
     * @return 工作流步骤
     */
    public OaFlowStep selectOaFlowStepById(Long id);

    /**
     * 查询工作流步骤列表
     * 
     * @param oaFlowStep 工作流步骤
     * @return 工作流步骤集合
     */
    public List<OaFlowStep> selectOaFlowStepList(OaFlowStep oaFlowStep);

    /**
     * 新增工作流步骤
     * 
     * @param oaFlowStep 工作流步骤
     * @return 结果
     */
    public int insertOaFlowStep(OaFlowStep oaFlowStep);

    /**
     * 修改工作流步骤
     * 
     * @param oaFlowStep 工作流步骤
     * @return 结果
     */
    public int updateOaFlowStep(OaFlowStep oaFlowStep);

    /**
     * 批量删除工作流步骤
     * 
     * @param ids 需要删除的工作流步骤ID
     * @return 结果
     */
    public int deleteOaFlowStepByIds(Long[] ids);

    /**
     * 删除工作流步骤信息
     * 
     * @param id 工作流步骤ID
     * @return 结果
     */
    public int deleteOaFlowStepById(Long id);
    
    
    /**
     * 测试工作流步骤
     * 
     * @param flowId Long 工作流ID
     * @return 结果
     */
    public int testOaFlowStep(Long flowId);
}
