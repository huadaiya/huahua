package com.huohuzhihui.oa.service;

import com.huohuzhihui.oa.domain.OaMeetingSummary;

import java.util.List;

/**
 * 会议记要Service接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface IOaMeetingSummaryService 
{
    /**
     * 查询会议记要
     * 
     * @param id 会议记要ID
     * @return 会议记要
     */
    public OaMeetingSummary selectOaMeetingSummaryById(Long id);

    /**
     * 查询会议记要列表
     * 
     * @param oaMeetingSummary 会议记要
     * @return 会议记要集合
     */
    public List<OaMeetingSummary> selectOaMeetingSummaryList(OaMeetingSummary oaMeetingSummary);

    /**
     * 新增会议记要
     * 
     * @param oaMeetingSummary 会议记要
     * @return 结果
     */
    public int insertOaMeetingSummary(OaMeetingSummary oaMeetingSummary);

    /**
     * 修改会议记要
     * 
     * @param oaMeetingSummary 会议记要
     * @return 结果
     */
    public int updateOaMeetingSummary(OaMeetingSummary oaMeetingSummary);

    /**
     * 批量删除会议记要
     * 
     * @param ids 需要删除的会议记要ID
     * @return 结果
     */
    public int deleteOaMeetingSummaryByIds(Long[] ids);

    /**
     * 删除会议记要信息
     * 
     * @param id 会议记要ID
     * @return 结果
     */
    public int deleteOaMeetingSummaryById(Long id);
}
