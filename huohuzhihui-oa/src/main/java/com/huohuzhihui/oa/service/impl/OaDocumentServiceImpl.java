package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.common.exception.CustomException;
import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.oa.domain.*;
import com.huohuzhihui.oa.mapper.OaDocumentMapper;
import com.huohuzhihui.oa.mapper.OaFlowMapper;
import com.huohuzhihui.oa.mapper.OaFlowStepMapper;
import com.huohuzhihui.oa.mapper.OaFormMapper;
import com.huohuzhihui.oa.service.IOaDocumentService;
import com.huohuzhihui.oa.service.IOaFlowProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 公文Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Service
public class OaDocumentServiceImpl implements IOaDocumentService 
{
    @Autowired
    private OaDocumentMapper oaDocumentMapper;
    @Autowired
    private OaFlowMapper oaFlowMapper;
    @Autowired
    private OaFormMapper oaFormMapper;
    @Autowired
    private OaFlowStepMapper oaFlowStepMapper;
    @Autowired
    private IOaFlowProcessService oaFlowProcessService;

    /**
     * 查询公文
     * 
     * @param id 公文ID
     * @return 公文
     */
    @Override
    public OaDocument selectOaDocumentById(Long id)
    {
    	OaDocument oaDocument = oaDocumentMapper.selectOaDocumentById(id);
    	OaForm fcond = new OaForm();
    	fcond.setDocumentId(id);
    	oaDocument.setForm(oaFormMapper.selectOaFormList(fcond));
    	return oaDocument;
    }

    /**
     * 查询公文列表
     * 
     * @param oaDocument 公文
     * @return 公文
     */
    @Override
    public List<OaDocument> selectOaDocumentList(OaDocument oaDocument)
    {
        return oaDocumentMapper.selectOaDocumentList(oaDocument);
    }

    /**
     * 新增公文
     * 
     * @param oaDocument 公文
     * @return 结果
     */
    @Override
    @Transactional
    public int insertOaDocument(OaDocument oaDocument)
    {
    	OaFlow flow = oaFlowMapper.selectOaFlowById(oaDocument.getFlowId());
    	if(flow == null || !"1".equals(flow.getStatus())) {
    		throw new CustomException("工作流不存在或者被禁用");
    	}
    	oaDocument.setAddTime(DateUtils.getNowDate());
    	oaDocument.setAddUser(SecurityUtils.getLoginUser().getUser().getUserId());
        int count = oaDocumentMapper.insertOaDocument(oaDocument);
        for(OaForm form:oaDocument.getForm()) {
        	if(form.getId() == null) {
        		form.setDocumentId(oaDocument.getId());
        		oaFormMapper.insertOaForm(form);
        	}else {
        		oaFormMapper.updateOaForm(form);
        	}
    		count ++;
    	}
    	if("1".equals(oaDocument.getStatus())) {
        	OaFlowStep scond = new OaFlowStep();
        	scond.setFlowId(oaDocument.getFlowId());
        	scond.setType("1");
        	List<OaFlowStep> step = oaFlowStepMapper.selectOaFlowStepList(scond);
        	if(step == null || step.size() < 1) {
        		throw new CustomException("工作流配置有误，没有起始流程");
        	}
        	oaDocument.setStepId(step.get(0).getId());
        	oaFlowProcessService.processStep(oaDocument, step.get(0));
        	oaDocumentMapper.updateOaDocument(oaDocument);
    	}
    	return count;
    }
    
    /**
     * 修改公文
     * 
     * @param oaDocument 公文
     * @return 结果
     */
    @Override
    public int updateOaDocument(OaDocument oaDocument)
    {
        oaDocument.setUpdateTime(DateUtils.getNowDate());
        int count = oaDocumentMapper.updateOaDocument(oaDocument);
        for(OaForm form:oaDocument.getForm()) {
        	if(form.getId() == null) {
        		form.setDocumentId(oaDocument.getId());
        		oaFormMapper.insertOaForm(form);
        	}else {
        		oaFormMapper.updateOaForm(form);
        	}
    		count ++;
    	}
    	return count;
    }

    /**
     * 批量删除公文
     * 
     * @param ids 需要删除的公文ID
     * @return 结果
     */
    @Override
    public int deleteOaDocumentByIds(Long[] ids)
    {
        return oaDocumentMapper.deleteOaDocumentByIds(ids);
    }

    /**
     * 删除公文信息
     * 
     * @param id 公文ID
     * @return 结果
     */
    @Override
    public int deleteOaDocumentById(Long id)
    {
        return oaDocumentMapper.deleteOaDocumentById(id);
    }
    
    /**
     * 撤销公文信息
     * 
     * @param id 公文ID
     * @return 结果
     */
    @Override
    @Transactional
    public int rollbackOaDocumentById(Long id)
    {
    	OaDocument oaDocument = oaDocumentMapper.selectOaDocumentById(id);
    	if(!"2".equals(oaDocument.getStatus())) {
    		throw new CustomException("只有办理中的公文才可以撤回");
    	}
    	OaFlowProcess fpc = new OaFlowProcess();
    	fpc.setDocumentId(id);
    	fpc.setStepId(oaDocument.getStepId());    	
    	List<OaFlowProcess> list = oaFlowProcessService.selectOaFlowProcessList(fpc);
    	for(OaFlowProcess p : list) {
    		if(!"1".equals(p.getStatus())) {
    			throw new CustomException("公文已经被签收或者处理，不能撤回，可联系相关人员进行退回");
    		}
    		oaFlowProcessService.deleteOaFlowProcessById(p.getId());
    	}
    	oaDocument.setStatus("8");
        return oaDocumentMapper.updateOaDocument(oaDocument);
    }
    

    /**
     * 发送公文
     * 
     * @param id 公文ID
     * @return 公文
     */
    @Override
    @Transactional
    public int sendOaDocument(Long id) {
    	OaDocument oaDocument = oaDocumentMapper.selectOaDocumentById(id);
    	if("0".equals(oaDocument.getStatus()) || "4".equals(oaDocument.getStatus()) || "8".equals(oaDocument.getStatus())) { //暂存/退回/撤销
    		
    		OaFlowStep scond = new OaFlowStep();
        	scond.setFlowId(oaDocument.getFlowId());
        	scond.setType("1");
        	List<OaFlowStep> step = oaFlowStepMapper.selectOaFlowStepList(scond);
        	if(step == null || step.size() < 1) {
        		throw new CustomException("工作流配置有误，没有起始流程");
        	}
        	oaDocument.setStepId(step.get(0).getId());
        	oaDocument.setStatus("1");
        	oaFlowProcessService.processStep(oaDocument, step.get(0));
        	oaDocument.setUpdateTime(DateUtils.getNowDate());
        	return oaDocumentMapper.updateOaDocument(oaDocument);
    	}else {
    		throw new CustomException("公文已经发送");
    	}
    }
}
