package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.oa.domain.OaForm;
import com.huohuzhihui.oa.mapper.OaFormMapper;
import com.huohuzhihui.oa.service.IOaFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 公文单Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Service
public class OaFormServiceImpl implements IOaFormService 
{
    @Autowired
    private OaFormMapper oaFormMapper;

    /**
     * 查询公文单
     * 
     * @param id 公文单ID
     * @return 公文单
     */
    @Override
    public OaForm selectOaFormById(Long id)
    {
        return oaFormMapper.selectOaFormById(id);
    }

    /**
     * 查询公文单列表
     * 
     * @param oaForm 公文单
     * @return 公文单
     */
    @Override
    public List<OaForm> selectOaFormList(OaForm oaForm)
    {
        return oaFormMapper.selectOaFormList(oaForm);
    }

    /**
     * 新增公文单
     * 
     * @param oaForm 公文单
     * @return 结果
     */
    @Override
    public int insertOaForm(OaForm oaForm)
    {
        return oaFormMapper.insertOaForm(oaForm);
    }

    /**
     * 修改公文单
     * 
     * @param oaForm 公文单
     * @return 结果
     */
    @Override
    public int updateOaForm(OaForm oaForm)
    {
        return oaFormMapper.updateOaForm(oaForm);
    }

    /**
     * 批量删除公文单
     * 
     * @param ids 需要删除的公文单ID
     * @return 结果
     */
    @Override
    public int deleteOaFormByIds(Long[] ids)
    {
        return oaFormMapper.deleteOaFormByIds(ids);
    }

    /**
     * 删除公文单信息
     * 
     * @param id 公文单ID
     * @return 结果
     */
    @Override
    public int deleteOaFormById(Long id)
    {
        return oaFormMapper.deleteOaFormById(id);
    }
}
