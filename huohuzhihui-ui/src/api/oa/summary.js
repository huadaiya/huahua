import request from '@/utils/request'

// 查询会议记要列表
export function listSummary(query) {
  return request({
    url: '/oa/summary/list',
    method: 'get',
    params: query
  })
}

// 查询会议记要详细
export function getSummary(id) {
  return request({
    url: '/oa/summary/' + id,
    method: 'get'
  })
}

// 新增会议记要
export function addSummary(data) {
  return request({
    url: '/oa/summary',
    method: 'post',
    data: data
  })
}

// 修改会议记要
export function updateSummary(data) {
  return request({
    url: '/oa/summary',
    method: 'put',
    data: data
  })
}

// 删除会议记要
export function delSummary(id) {
  return request({
    url: '/oa/summary/' + id,
    method: 'delete'
  })
}

// 导出会议记要
export function exportSummary(query) {
  return request({
    url: '/oa/summary/export',
    method: 'get',
    params: query
  })
}