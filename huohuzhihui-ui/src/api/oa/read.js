import request from '@/utils/request'

// 查询轮阅记录列表
export function listRead(query) {
  return request({
    url: '/oa/read/list',
    method: 'get',
    params: query
  })
}


// 查询个人轮阅记录列表
export function myRead(query) {
  return request({
    url: '/oa/read/my',
    method: 'get',
    params: query
  })
}

// 查询轮阅记录详细
export function getRead(id) {
  return request({
    url: '/oa/read/' + id,
    method: 'get'
  })
}

// 新增轮阅记录
export function addRead(data) {
  return request({
    url: '/oa/read',
    method: 'post',
    data: data
  })
}

// 修改轮阅记录
export function updateRead(data) {
  return request({
    url: '/oa/read',
    method: 'put',
    data: data
  })
}

// 删除轮阅记录
export function delRead(id) {
  return request({
    url: '/oa/read/' + id,
    method: 'delete'
  })
}

// 标记已读
export function markRead(id) {
  return request({
    url: '/oa/read/mark/' + id,
    method: 'get'
  })
}


// 导出轮阅记录
export function exportRead(query) {
  return request({
    url: '/oa/read/export',
    method: 'get',
    params: query
  })
}
