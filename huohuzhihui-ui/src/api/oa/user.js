import request from '@/utils/request'

// 查询与会人员列表
export function listUser(query) {
  return request({
    url: '/oa/user/list',
    method: 'get',
    params: query
  })
}

// 查询与会人员详细
export function getUser(id) {
  return request({
    url: '/oa/user/' + id,
    method: 'get'
  })
}

// 新增与会人员
export function addUser(data) {
  return request({
    url: '/oa/user',
    method: 'post',
    data: data
  })
}

// 修改与会人员
export function updateUser(data) {
  return request({
    url: '/oa/user',
    method: 'put',
    data: data
  })
}

// 删除与会人员
export function delUser(id) {
  return request({
    url: '/oa/user/' + id,
    method: 'delete'
  })
}

// 导出与会人员
export function exportUser(query) {
  return request({
    url: '/oa/user/export',
    method: 'get',
    params: query
  })
}