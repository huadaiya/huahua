import request from '@/utils/request'

// 查询会议室列表
export function listDivan(query) {
  return request({
    url: '/oa/divan/list',
    method: 'get',
    params: query
  })
}


// 查询会议室列表
export function selectDivan(query) {
  return request({
    url: '/oa/divan/select',
    method: 'get',
    params: query
  })
}


// 查询会议室详细
export function getDivan(id) {
  return request({
    url: '/oa/divan/' + id,
    method: 'get'
  })
}

// 新增会议室
export function addDivan(data) {
  return request({
    url: '/oa/divan',
    method: 'post',
    data: data
  })
}

// 修改会议室
export function updateDivan(data) {
  return request({
    url: '/oa/divan',
    method: 'put',
    data: data
  })
}

// 删除会议室
export function delDivan(id) {
  return request({
    url: '/oa/divan/' + id,
    method: 'delete'
  })
}

// 导出会议室
export function exportDivan(query) {
  return request({
    url: '/oa/divan/export',
    method: 'get',
    params: query
  })
}