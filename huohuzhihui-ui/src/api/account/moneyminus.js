import request from '@/utils/request'

// 查询帐户减款列表
export function listMoneyminus(query) {
  return request({
    url: '/account/moneyminus/list',
    method: 'get',
    params: query
  })
}

// 查询帐户减款详细
export function getMoneyminus(id) {
  return request({
    url: '/account/moneyminus/' + id,
    method: 'get'
  })
}

// 新增帐户减款
export function addMoneyminus(data) {
  return request({
    url: '/account/moneyminus',
    method: 'post',
    data: data
  })
}

// 修改帐户减款
export function updateMoneyminus(data) {
  return request({
    url: '/account/moneyminus',
    method: 'put',
    data: data
  })
}

// 删除帐户减款
export function delMoneyminus(id) {
  return request({
    url: '/account/moneyminus/' + id,
    method: 'delete'
  })
}

// 导出帐户减款
export function exportMoneyminus(query) {
  return request({
    url: '/account/moneyminus/export',
    method: 'get',
    params: query
  })
}