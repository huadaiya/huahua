import request from '@/utils/request'

// 查询班级列表
export function listClazz(query) {
  return request({
    url: '/base/clazz/list',
    method: 'get',
    params: query
  })
}

// 查询班级列表
export function selectClazz(query) {
  return request({
    url: '/base/clazz/select',
    method: 'get',
    params: query
  })
}

// 查询班主任班级列表
export function listMasterClazz(query) {
  return request({
    url: '/base/clazz/master',
    method: 'get',
    params: query
  })
}

// 查询班级详细
export function getClazz(id) {
  return request({
    url: '/base/clazz/' + id,
    method: 'get'
  })
}

// 新增班级
export function addClazz(data) {
  return request({
    url: '/base/clazz',
    method: 'post',
    data: data
  })
}

// 修改班级
export function updateClazz(data) {
  return request({
    url: '/base/clazz',
    method: 'put',
    data: data
  })
}

// 删除班级
export function delClazz(id) {
  return request({
    url: '/base/clazz/' + id,
    method: 'delete'
  })
}

// 导出班级
export function exportClazz(query) {
  return request({
    url: '/base/clazz/export',
    method: 'get',
    params: query
  })
}


// 启用班级
export function enableClazz(id) {
  return request({
    url: '/base/clazz/enable/' + id,
    method: 'get'
  })
}


// 禁用班级
export function disableClazz(id) {
  return request({
    url: '/base/clazz/disable/' + id,
    method: 'get'
  })
}

// 毕业班级
export function graduationClazz(id) {
  return request({
    url: '/base/clazz/graduation/' + id,
    method: 'get'
  })
}

// 下载导入模板
export function importTemplate() {
  return request({
    url: '/base/clazz/importTemplate',
    method: 'get'
  })
}
