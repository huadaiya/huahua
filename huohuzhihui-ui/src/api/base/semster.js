import request from '@/utils/request'

// 查询学期列表
export function listSemster(query) {
  return request({
    url: '/base/semster/list',
    method: 'get',
    params: query
  })
}


// 查询学期列表
export function selectSemster(query) {
  return request({
    url: '/base/semster/select',
    method: 'get',
    params: query
  })
}

// 查询学期详细
export function getSemster(id) {
  return request({
    url: '/base/semster/' + id,
    method: 'get'
  })
}

// 新增学期
export function addSemster(data) {
  return request({
    url: '/base/semster',
    method: 'post',
    data: data
  })
}

// 修改学期
export function updateSemster(data) {
  return request({
    url: '/base/semster',
    method: 'put',
    data: data
  })
}

// 删除学期
export function delSemster(id) {
  return request({
    url: '/base/semster/' + id,
    method: 'delete'
  })
}

// 导出学期
export function exportSemster(query) {
  return request({
    url: '/base/semster/export',
    method: 'get',
    params: query
  })
}