import request from '@/utils/request'

// 查询学生列表
export function listStudent(query) {
  return request({
    url: '/base/student/list',
    method: 'get',
    params: query
  })
}

// 查询学生列表
export function selectStudent(query) {
  return request({
    url: '/base/student/select',
    method: 'get',
    params: query
  })
}

// 查询学生详细
export function getStudent(id) {
  return request({
    url: '/base/student/' + id,
    method: 'get'
  })
}

// 新增学生
export function addStudent(data) {
  return request({
    url: '/base/student',
    method: 'post',
    data: data
  })
}

// 修改学生
export function updateStudent(data) {
  return request({
    url: '/base/student',
    method: 'put',
    data: data
  })
}

// 删除学生
export function delStudent(id) {
  return request({
    url: '/base/student/' + id,
    method: 'delete'
  })
}

// 导出学生
export function exportStudent(query) {
  return request({
    url: '/base/student/export',
    method: 'get',
    params: query
  })
}

// 启用学生
export function enableClazz(id) {
  return request({
    url: '/base/student/enable/' + id,
    method: 'get'
  })
}


// 禁用学生
export function disableClazz(id) {
  return request({
    url: '/base/student/disable/' + id,
    method: 'get'
  })
}

// 下载导入模板
export function importTemplate() {
  return request({
    url: '/base/student/importTemplate',
    method: 'get'
  })
}