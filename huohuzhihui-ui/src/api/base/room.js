import request from '@/utils/request'

// 查询房间列表
export function listRoom(query) {
  return request({
    url: '/base/room/list',
    method: 'get',
    params: query
  })
}

//下拉列表
export function selectRoom(query) {
  return request({
    url: '/base/room/select',
    method: 'get',
    params: query
  })
}

//选择固定教室
export function selectClassRoom(query) {
  return request({
    url: '/base/room/clazz',
    method: 'get',
    params: query
  })
}

// 查询房间详细
export function getRoom(id) {
  return request({
    url: '/base/room/' + id,
    method: 'get'
  })
}

// 新增房间
export function addRoom(data) {
  return request({
    url: '/base/room',
    method: 'post',
    data: data
  })
}

// 修改房间
export function updateRoom(data) {
  return request({
    url: '/base/room',
    method: 'put',
    data: data
  })
}

// 删除房间
export function delRoom(id) {
  return request({
    url: '/base/room/' + id,
    method: 'delete'
  })
}

// 导出房间
export function exportRoom(query) {
  return request({
    url: '/base/room/export',
    method: 'get',
    params: query
  })
}

// 下载导入模板
export function importTemplate() {
  return request({
    url: '/base/room/importTemplate',
    method: 'get'
  })
}