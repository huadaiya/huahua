package com.huohuzhihui.door.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.door.domain.DoorDevice;
import com.huohuzhihui.door.service.IDoorDeviceService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 门禁设备Controller
 * 
 * @author huohuzhihui
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/door/device")
public class DoorDeviceController extends BaseController
{
    @Autowired
    private IDoorDeviceService doorDeviceService;

    /**
     * 查询门禁设备列表
     */
    @PreAuthorize("@ss.hasPermi('door:device:list')")
    @GetMapping("/list")
    public TableDataInfo list(DoorDevice doorDevice)
    {
        startPage();
        List<DoorDevice> list = doorDeviceService.selectDoorDeviceList(doorDevice);
        return getDataTable(list);
    }

    /**
     * 导出门禁设备列表
     */
    @PreAuthorize("@ss.hasPermi('door:device:export')")
    @Log(title = "门禁设备", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DoorDevice doorDevice)
    {
        List<DoorDevice> list = doorDeviceService.selectDoorDeviceList(doorDevice);
        ExcelUtil<DoorDevice> util = new ExcelUtil<DoorDevice>(DoorDevice.class);
        return util.exportExcel(list, "device");
    }

    /**
     * 获取门禁设备详细信息
     */
    @PreAuthorize("@ss.hasPermi('door:device:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(doorDeviceService.selectDoorDeviceById(id));
    }

    /**
     * 新增门禁设备
     */
    @PreAuthorize("@ss.hasPermi('door:device:add')")
    @Log(title = "门禁设备", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DoorDevice doorDevice)
    {
        return toAjax(doorDeviceService.insertDoorDevice(doorDevice));
    }

    /**
     * 修改门禁设备
     */
    @PreAuthorize("@ss.hasPermi('door:device:edit')")
    @Log(title = "门禁设备", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DoorDevice doorDevice)
    {
        return toAjax(doorDeviceService.updateDoorDevice(doorDevice));
    }

    /**
     * 删除门禁设备
     */
    @PreAuthorize("@ss.hasPermi('door:device:remove')")
    @Log(title = "门禁设备", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(doorDeviceService.deleteDoorDeviceByIds(ids));
    }
}
